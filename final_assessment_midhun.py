class Pet:
    spec=['dog','cat','horse','hamster']
    def __init__(self,species=None,name=" "):
        try:
            i=Pet.spec.index(species)
            self.species=species
            self.name=name
        except ValueError:
            print('Invalid Species')
    def __str__(self):
        try:
            if self.name=="":
                return f'Species Of {self.species},unnamed'
            else:
                return f'Species of {self.species},named {self.name}'
        except:
            return 'This can not be added'
class Dog(Pet):
    def __init__(self,name="",chases='Cats'):
        super().__init__('dog',name)
        self.name=name
        self.chases=chases
    def __str__(self):
        print(Pet.__str__(self))
        return f'chases {self.chases}'
class Cat(Pet):
    def __init__(self,name="",hates='Dogs'):
        super().__init__('cat',name)
        self.name=name
        self.hates=hates
    def __str__(self):
        print(Pet.__str__(self))
        return f'hates {self.hates}'
    
        

def main():
    print('\n')
    print('1.Crete And Display Pet')
    print('2.Create Dog')
    print('3.Create Cat')
    n=int(input())
    if n==1:
        print('Enter Pet Species')
        pet_s=str(input())
        print('Enter Pet Name')
        pet_n=str(input())
        p=Pet(pet_s,pet_n)
        print(p)
    if n==2:
        print('Enter Dog Name')
        dog_n=str(input())
        print('Whom does he chase')
        dog_c=str(input())
        d=Dog(dog_n,dog_c)
        print(d)
    if n==3:
        print('Enter Cat Name')
        cat_n=str(input())
        print('Whom does he hate')
        cat_h=str(input())
        c=Cat(cat_n,cat_h)
        print(c)
        
while True:
    main()
        
        
        
        





    
